package com.example.linuxcontributors;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;

public class DownloadTask extends AsyncTask<Integer, Integer, List<JSONObject>> {
	private static final String URL = "https://api.github.com/repos/torvalds/linux/contributors";
	private ILoadedContributors callback;

	public DownloadTask(ILoadedContributors callback) {
		this.callback = callback;
	}

	@Override
	protected List<JSONObject> doInBackground(Integer... params) {
		try {
			List<JSONObject> list = new ArrayList<>();
			JSONArray rootArray = new JSONArray(IOUtils.toString(new URL(URL)));
			for (int i = 0; i < rootArray.length(); i++) {
				list.add(rootArray.getJSONObject(i));
			}
			return list;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(List<JSONObject> result) {
		callback.handleJsonContributors(result);
	}
	
}
