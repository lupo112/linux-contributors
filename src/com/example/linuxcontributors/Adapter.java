package com.example.linuxcontributors;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class Adapter extends BaseAdapter {
	protected LayoutInflater layoutInflater;
	private List<JSONObject> list = new ArrayList<>();

	public Adapter(Context context) {
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setList(List<JSONObject> list) {
		this.list = list;
		notifyDataSetChanged();
	}
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public JSONObject getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View v, ViewGroup parent) {
		JSONObject item = getItem(position);
		if (v == null) {
			v = layoutInflater.inflate(R.layout.contributor_row, parent, false);
		}
		// TODO viewholder
		
		((TextView)v.findViewById(R.id.login)).setText(item.optString("login"));
		((TextView)v.findViewById(R.id.url)).setText(item.optString("html_url"));
		return v;
	}

}
