package com.example.linuxcontributors;

import java.util.List;

import org.json.JSONObject;

public interface ILoadedContributors {
	public void handleJsonContributors(List<JSONObject> list);
}
