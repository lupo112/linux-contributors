package com.example.linuxcontributors;

import java.util.List;

import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

public class MainActivity extends ListActivity implements ILoadedContributors {
	private Adapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		adapter = new Adapter(this);
		setListAdapter(adapter);
		
		DownloadTask jsonTask = new DownloadTask(this);
		jsonTask.execute();
	}

	@Override
	public void handleJsonContributors(List<JSONObject> list) {
		findViewById(R.id.loading).setVisibility(View.GONE);
		if (list == null) {
			findViewById(R.id.list_empty).setVisibility(View.VISIBLE);
		}
		else {
			adapter.setList(list);
			findViewById(android.R.id.list).setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		JSONObject item = adapter.getItem(position);
		String url = item.optString("html_url");
		startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
	}

}
